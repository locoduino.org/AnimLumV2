/*Copyright 2015  G. Marty
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.*/


#ifndef AnimLumV2.h
#define AnimLumV2.h

#include <Arduino.h>
#include <ScheduleTable.h>


//-------------------class led--------------------
class Led : public ScheduleTableAction
{
  private:
    byte pin;
    boolean eOFF=LOW;
    boolean eON=HIGH;
  public:
    Led();
    void setup(byte apin, boolean amontageinverse=0);
    void on();
    void off();
    void action();
};

//--------------------class chenillard----------------------------

class Chenillard : public ScheduleTableAction
{
  private:
    Led *led;
    byte nombre;
    byte compteur = 0;
  public:
    Chenillard();
    void setup(byte anombre, byte abroche[], boolean amontageinverse=0);
    void action();
    void off();
};

//------------------class feu--------------------------------------
class Feu  : public ScheduleTableAction
{
  private:
    Led led[3];
    byte compteur = 0;
  public:
    Feu();
    void setup(byte abroche[], boolean amontageinverse=0);
    void action();
    void off();
};



#endif
