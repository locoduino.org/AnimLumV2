/*Copyright 2015  G. Marty
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.*/

#include "AnimLumV2.h"

#include <Arduino.h>

//---------------------class led-----------------------------
Led::Led()
{
}

void Led::setup(byte apin, boolean amontageinverse)
{
  pin=apin;
  pinMode(pin, OUTPUT);
  if(amontageinverse==1){eOFF=HIGH; eON=LOW;}
  off();
}

void Led::on()
{
  digitalWrite(pin, eON);
}

void Led::off()
{
  digitalWrite(pin, eOFF);
}

void Led::action(){
  digitalWrite(pin, !digitalRead(pin));
}


//-----------------------class chenillard-------------------------------------

void Chenillard::off()
{
  for (byte i = 0; i < nombre; i++) led[i].off();
}

void Chenillard::action()
{

    led[compteur].off();
    compteur++;
    if (compteur > (nombre-1)) compteur = 0;
    led[compteur].on();
}

Chenillard::Chenillard(){
}

void Chenillard::setup(byte anombre, byte abroche[], boolean amontageinverse){
  nombre=anombre;
  led=new Led[nombre];
  for(byte i=0;i<nombre;i++){led[i].setup(abroche[i], amontageinverse);}
  led[0].on();
}

//-----------------------------class Feu------------------------------
Feu::Feu()
{
}

void Feu::setup(byte abroche[], boolean amontageinverse){
  for(byte i=0;i<3;i++){led[i].setup(abroche[i], amontageinverse);}
  off();
  led[0].on();
}

void Feu::action()
{
  compteur++;
  switch(compteur)
  {
  case 1:
  led[0].off();
  led[1].on();
  break;
  case 2:
  led[1].off();
  led[2].on();
  break;
  case 3:
  led[2].off();
  led[0].on();
  compteur = 0;
  break;
  }
}

void Feu::off()
{
  for(byte i=0;i<3;i++){led[i].off();}
}
