# Bibliothèque d'animations lumineuses utilisant la bibliothèque ScheduleTable pour gérer le temps

*Copyright 2015  G. Marty
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.*

* Bibliothèque AnimLumV1 : [dépôt][1]
* Bibliothèque ScheduleTable : [dépôt][2], [Tutoriel][3]

## Introduction
Ce qui change par rapport à la version 1 :
* la gestion du temps : elle n'est plus faite intrinséquement dans l'objet. Elle sera faite par le code du programme via la bibliothèque ScheduleTable. L'avantage sera une meilleure gestion dans le sens où c'est vous qui décidez quel timing faire. Par exemple, dans le cas d'un feu tricolore, voous déciderez du temps de chaque feu, il sera possible de faire un feu plus ou moins long ; de même pour les autres animations.
* le désavantage : l'absence d'animations utilisant l'aléatoire. Il est en effet impossible de décider d'un temps aléatoire avec la bibliothèque ScheduleTable.
* l'autre désavantage est que les principes de code par rapport à la version 1 sont différents.
* la difficulté sera de maîtriser deux bibliothèques. Si cela vous semble difficile, dirigez-vous vers [la version 1][1].

## Principes généraux
Pour chaque animation, 2 objets sont à déclarer : l'objet animation correspondant à ce que vous voulez et une table pour gérer le temps de l'animation.  
Le setup de chaque objet animation est identique avec le changement de version. Selon les animations, la méthode setup peut comprendre le nombre de del, la broche ou le tableau de broches nécessaires à l'animation et la méthode pour relier la del et la carte Arduino.  
Ensuite, il sera nécessaire d'ajouter l'objet animation qui est dérivé de la bibliothèque ScheduleTable à la table qui a été créée.  

Sur le principe, la méthode action de chaque objet animation donne le tac à chaque étape de l'animation, méthode action qui sera appelée à chaque tic de la table créée.

## Spécificités de chaque animation

Vous trouverez un exemple de programme pour chaque animation.

### Led
Aucune difficulté pour cette animation simple. La méthode setup accepte comme paramètre le numéro de la broche et 1 si le montage est inversé (entre 5V et la broche).

### Feu tricolore
Contrairement à la version 1, cette animation comprend un seul feu tricolore. Il sera facile d'en rajouter un ou 2 dans le carrefour avec donc 2 ou 3 objets animation dans la même table. La méthode setup accepte le tableau de numéros de broche {rouge, vert, orange} et 1 si le montage est inversé (entre 5V et la broche).

### Chenillard
La méthode setup accepte comme paramètre le nombre de DEL, le tableau des numéros de broches et 1 si le montage est inversé (entre 5V et la broche).

[1]: https://git.framasoft.org/locoduino.org/AnimLum
[2]: https://git.framasoft.org/locoduino.org/ScheduleTable
[3]: http://www.locoduino.org/spip.php?article116
