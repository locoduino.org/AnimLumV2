//Exemple pour l'animation chenillard

#include <AnimLumV2.h>
#include <ScheduleTable.h>

Chenillard del;
ScheduleTable cycle(4, 3000);
byte table[]={4,6,9,12};

void setup() {
  del.setup(4, table);
  cycle.at(500, del);
  cycle.at(800, del);
  cycle.at(1200, del);
  cycle.at(2200, del);
  cycle.start();
}

void loop() {
  ScheduleTable::update();
}
