//Exemple pour l'animation del

#include <AnimLumV2.h>
#include <ScheduleTable.h>

Led del;
ScheduleTable cycle(1, 1000);

void setup() {
  del.setup(13);
  cycle.at(500, del);
  cycle.start();
}

void loop() {
  ScheduleTable::update();
}
