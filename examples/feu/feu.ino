//Exemple pour l'animation feu

#include <AnimLumV2.h>
#include <ScheduleTable.h>

Feu del;
ScheduleTable cycle(3, 3000);
byte table[]={6,9,12};

void setup() {
  del.setup(table);
  cycle.at(500, del);
  cycle.at(800, del);
  cycle.at(1200, del);
  cycle.start();
}

void loop() {
  ScheduleTable::update();
}
